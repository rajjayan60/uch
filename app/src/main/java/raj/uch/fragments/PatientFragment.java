package raj.uch.fragments;

/**
 * Created by rajeshj on 7/20/2016.
 */
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import raj.uch.R;
import raj.uch.adapters.ChatSection;
import raj.uch.adapters.ChatSectionAdapter;
import raj.uch.models.ChatItem;


public class PatientFragment extends Fragment{

    private RecyclerView mPatientRecyclerView;

    public PatientFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_patient, container, false);
        mPatientRecyclerView = (RecyclerView) view.findViewById(R.id.patient_recycler_view);

        mPatientRecyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mPatientRecyclerView.setLayoutManager(mLayoutManager);
        mPatientRecyclerView.setItemAnimator(new DefaultItemAnimator());

        List<ChatItem> aList    = new ArrayList<>();
        List<ChatItem> bList    = new ArrayList<>();
        List<ChatItem> cList    = new ArrayList<>();

        ChatItem a = new ChatItem();
        a.setName("Amit Kumar");
        a.setProfileImage("http://graph.facebook.com/1034362036574106/picture?type=large");
        a.setDepartment("");
        aList.add(a);

        a = new ChatItem();
        a.setName("Andy Sim");
        a.setProfileImage("http://graph.facebook.com/10204513679873903/picture?type=large");
        a.setDepartment("");
        aList.add(a);

        a = new ChatItem();
        a.setName("Alan Cruz");
        a.setProfileImage("http://graph.facebook.com/10208320220553426/picture?type=large");
        a.setDepartment("");
        aList.add(a);

        ChatItem b = new ChatItem();
        b.setName("Bryan Teo");
        b.setProfileImage("http://graph.facebook.com/815743268548887/picture?type=large");
        b.setDepartment("");
        bList.add(b);

        b = new ChatItem();
        b.setName("Ben Loh");
        b.setProfileImage("http://graph.facebook.com/10207178112423923/picture?type=large");
        b.setDepartment("");
        bList.add(b);

        b = new ChatItem();
        b.setName("BK Thian Ricky");
        b.setProfileImage("http://graph.facebook.com/744083815728630/picture?type=large");
        b.setDepartment("");
        bList.add(b);

        ChatItem c = new ChatItem();
        c.setName("Calvin Yap");
        c.setProfileImage("http://graph.facebook.com/10153786766467529/picture?type=large");
        c.setDepartment("");
        cList.add(c);

        c = new ChatItem();
        c.setName("Corneles Jaemes");
        c.setProfileImage("http://graph.facebook.com/934259936650094/picture?type=large");
        c.setDepartment("");
        cList.add(c);

        c = new ChatItem();
        c.setName("Chinghwee Low");
        c.setProfileImage("http://graph.facebook.com/1064469110285688/picture?type=large");
        c.setDepartment("");
        cList.add(c);

        ChatSection aSection = new ChatSection("A", aList);
        ChatSection bSection = new ChatSection("B", bList);
        ChatSection cSection = new ChatSection("C", cList);

        List<ChatSection> sections = Arrays.asList(aSection, bSection, cSection);

        ChatSectionAdapter adapter = new ChatSectionAdapter(getActivity(), sections);

        mPatientRecyclerView.setAdapter(adapter);
        adapter.expandAllParents();
        adapter.notifyDataSetChanged();

        return view;

    }

}
