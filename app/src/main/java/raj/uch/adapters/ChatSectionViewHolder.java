package raj.uch.adapters;

import android.view.View;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;

import raj.uch.R;

public class ChatSectionViewHolder extends ParentViewHolder {

    private TextView mSectionHeaderTextView;

    public ChatSectionViewHolder(View itemView) {
        super(itemView);
        mSectionHeaderTextView = (TextView) itemView.findViewById(R.id.chat_section_title);
    }

    public void bind(ChatSection section) {
        mSectionHeaderTextView.setText(section.getName());
    }

    @Override
    public boolean shouldItemViewClickToggleExpansion() {
        return false;
    }
}
