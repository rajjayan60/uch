package raj.uch.adapters;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.util.List;

import raj.uch.models.ChatItem;

public class ChatSection implements ParentListItem {

    private List<ChatItem> mChatList;
    private String mName;

    public ChatSection(String name, List chatList) {
        mChatList = chatList;
        mName = name;
    }

    @Override
    public List getChildItemList() {
        return mChatList;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }

    public String getName(){
        return mName;
    }

}

