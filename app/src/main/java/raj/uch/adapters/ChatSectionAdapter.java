package raj.uch.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.util.List;

import raj.uch.R;
import raj.uch.models.ChatItem;

public class ChatSectionAdapter extends ExpandableRecyclerAdapter<ChatSectionViewHolder, ChatViewHolder> {

    private LayoutInflater mInflator;

    public ChatSectionAdapter(Context context, List<ChatSection> parentItemList) {
        super(parentItemList);
        mInflator = LayoutInflater.from(context);
    }

    // onCreate ...
    @Override
    public ChatSectionViewHolder onCreateParentViewHolder(ViewGroup parentViewGroup) {
        View sectionView = mInflator.inflate(R.layout.chat_header_section, parentViewGroup, false);
        return new ChatSectionViewHolder(sectionView);
    }

    @Override
    public ChatViewHolder onCreateChildViewHolder(ViewGroup childViewGroup) {
        View peopleView = mInflator.inflate(R.layout.chat_item_row, childViewGroup, false);
        return new ChatViewHolder(peopleView);
    }

    // onBind ...
    @Override
    public void onBindParentViewHolder(ChatSectionViewHolder sectionViewHolder, int position, ParentListItem parentListItem) {
        ChatSection section = (ChatSection) parentListItem;
        sectionViewHolder.bind(section);
    }

    @Override
    public void onBindChildViewHolder(ChatViewHolder peopleViewHolder, int position, Object childListItem) {
        ChatItem chatItem = (ChatItem) childListItem;
        peopleViewHolder.bind(chatItem);
    }
}

