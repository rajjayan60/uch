package raj.uch.adapters;

import android.view.View;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;

import de.hdodenhof.circleimageview.CircleImageView;
import raj.uch.R;
import raj.uch.models.ChatItem;


public class ChatViewHolder extends ChildViewHolder {

    private View mItemView;

    private TextView mName;
    private TextView mDepartment;
    private de.hdodenhof.circleimageview.CircleImageView mImage;

    public ChatViewHolder(View itemView) {
        super(itemView);
        mItemView     = itemView;
        mName         = (TextView) itemView.findViewById(R.id.person_name);
        mDepartment   = (TextView) itemView.findViewById(R.id.person_department);
        mImage        = (CircleImageView) itemView.findViewById(R.id.person_image);
        mItemView.setClickable(true);
    }

    public void bind(final ChatItem chatItem) {
        mName.setText(chatItem.getName());
        mDepartment.setText(chatItem.getDepartment());

        if(String.valueOf(chatItem.getDepartment()).equals(""))
        {
            mDepartment.setVisibility(View.GONE);
        }
        else
        {
            mDepartment.setVisibility(View.VISIBLE);
        }

        Glide.with(mImage.getContext())
                .load(chatItem.getProfileImage())
                .priority(Priority.NORMAL)
                .centerCrop()
                .crossFade()
                .into(mImage);

        mItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Bundle args = new Bundle();
//                args.putInt(Constants.USER_ID, person.getId());
//                args.putString(Constants.USER_NAME, person.getName());
//                args.putString(Constants.USER_BIO, person.getBio());
//                args.putString(Constants.USER_IMAGE, person.getProfileImage());
//                args.putInt(Constants.PROFILE_FOLLOWERS, person.getProfileFollowers());
//                args.putInt(Constants.PROFILE_FOLLOWING, person.getProfileFollowing());
//
//                Intent i = new Intent(v.getContext(), ProfileActivity.class);
//                i.putExtras(args);
//                v.getContext().startActivity(i);

            }
        });

    }

}

