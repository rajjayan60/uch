package raj.uch.fragments;

/**
 * Created by rajeshj on 7/20/2016.
 */
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import raj.uch.R;
import raj.uch.adapters.ChatSection;
import raj.uch.adapters.ChatSectionAdapter;
import raj.uch.models.ChatItem;


public class DoctorFragment extends Fragment{

    private RecyclerView mDoctorRecyclerView;

    public DoctorFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_doctor, container, false);
        mDoctorRecyclerView = (RecyclerView) view.findViewById(R.id.doctor_recycler_view);

        mDoctorRecyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mDoctorRecyclerView.setLayoutManager(mLayoutManager);
        mDoctorRecyclerView.setItemAnimator(new DefaultItemAnimator());

        List<ChatItem> aList    = new ArrayList<>();
        List<ChatItem> bList    = new ArrayList<>();
        List<ChatItem> cList    = new ArrayList<>();

        ChatItem a = new ChatItem();
        a.setName("Dr. Angie Ang");
        a.setProfileImage("http://graph.facebook.com/218771281817058/picture?type=large");
        a.setDepartment("Dentist");
        aList.add(a);

        a = new ChatItem();
        a.setName("Dr. Avinash Kumar");
        a.setProfileImage("http://graph.facebook.com/10153827652343122/picture?type=large");
        a.setDepartment("Optometrist");
        aList.add(a);

        a = new ChatItem();
        a.setName("Dr. Andy Lee");
        a.setProfileImage("http://graph.facebook.com/1334116736601688/picture?type=large");
        a.setDepartment("Cardiologist");
        aList.add(a);

        ChatItem b = new ChatItem();
        b.setName("Dr. Bernard Tan");
        b.setProfileImage("http://graph.facebook.com/10209535898817999/picture?type=large");
        b.setDepartment("Dermatologist");
        bList.add(b);

        b = new ChatItem();
        b.setName("Dr. Brian Poah");
        b.setProfileImage("http://graph.facebook.com/10204843074018363/picture?type=large");
        b.setDepartment("Nuerologist");
        bList.add(b);

        b = new ChatItem();
        b.setName("Prof. Bella Nessya");
        b.setProfileImage("http://graph.facebook.com/10206449895070288/picture?type=large");
        b.setDepartment("Psycologist");
        bList.add(b);

        ChatItem c = new ChatItem();
        c.setName("Dr. Cedar Vanderpot");
        c.setProfileImage("http://graph.facebook.com/1011089348913208/picture?type=large");
        c.setDepartment("ENT");
        cList.add(c);

        c = new ChatItem();
        c.setName("Dr. Cedric Hoon");
        c.setProfileImage("http://graph.facebook.com/10153641796038503/picture?type=large");
        c.setDepartment("General");
        cList.add(c);


        ChatSection aSection = new ChatSection("A", aList);
        ChatSection bSection = new ChatSection("B", bList);
        ChatSection cSection = new ChatSection("C", cList);

        List<ChatSection> sections = Arrays.asList(aSection, bSection, cSection);

        ChatSectionAdapter adapter = new ChatSectionAdapter(getActivity(), sections);

        mDoctorRecyclerView.setAdapter(adapter);
        adapter.expandAllParents();
        adapter.notifyDataSetChanged();

        // Inflate the layout for this fragment
        return view;
    }

}
